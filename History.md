1.1.1-RIDDLER / 2018-07-18
==================

  * Update README.md
  * Update Jacoco Version
  * Update .gitlab-ci.yml
  * Enable assertions in tests
  * Add LICENSE
  * Merge branch 'matrix-improvements' into 'master'
  * Round five... 8(
  * Round four...
  * Round three
  * Second try to test with octave
  * Try to test using octave
  * Add Matrix#solve() tests
  * Implement Matrix#solve()
  * Adapt test to changes in Row
  * Add and document some Row methods
  * Adapt test to changes in Column
  * Add and document some Column methods
  * Minor improvements to Matrix
  * Minor toString() improvemnt
  * Fix tests when no examples are present
  * Extract examples
  * Add .gitlab-ci.yml
  * Fix JavaDoc errors

1.1.0-RIDDLER / 2018-07-08
==========================

  * Change version number
  * Merge branch 'misc-changes' into 'master'
  * Add `@Override` annotations Task #733 - Mejoras de código
  * Finalize a lot of variables Task #733 - Mejoras de código
  * Finalize a lot of variables Task #733 - Mejoras de código
  * Don't use keys to iterate over map
  * Use `%n` instead of `\n` Task #733 - Mejoras de código
  * Close streams and finalize lot's of variables Task #733 - Mejoras de código
  * Don't iterate using keys Task #733 - Mejoras de código
  * Close most streams Task #733 - Mejoras de código
  * Close streams and use Entries instead of get(key) Task #733 - Mejoras de código
  * Make some attributes final Task #733 - Mejoras de código
  * Use enhanced Loop Task #735 - No utilizar colecciones obsoletas
  * Exchange StringBuffer with StringBuilder Task #735 - No utilizar colecciones obsoletas
  * Use isEmpty() instead of size() == 0 Task #735 - No utilizar colecciones obsoletas
  * Adapt ant demos Task #735 - No utilizar colecciones obsoletas
  * Remove usages of Vector Task #735 - No utilizar colecciones obsoletas
  * Merge branch 'tabs-spaces' into 'master'
  * Disable MySQL test temporarily
  * Cambios en los tests
  * Cambios en el paquete libai.demos.* Task #732 - Cambiar [TAB] por [SPACE]
  * Cambios varios que faltaron Task #732 - Cambiar [TAB] por [SPACE]
  * Cambios en el paquete libai.search.* Task #732 - Cambiar [TAB] por [SPACE]
  * Cambios en el paquete libai.nn.* Task #732 - Cambiar [TAB] por [SPACE]
  * Cambios en el paquete libai.io.*
  * Cambios en el paquete libai.genetics.* Task #732 - Cambiar [TAB] por [SPACE]
  * Cambios en el paquete libai.fuzzy.* Task #732 - Cambiar [TAB] por [SPACE]
  * Cambios en el paquete libai.common.* Task #732 - Cambiar [TAB] por [SPACE]
  * Cambios en el paquete libai.classifiers.* Task #732 - Cambiar [TAB] por [SPACE]
  * Cambios en el paquete libai.classifiers Task #732 - Cambiar [TAB] por [SPACE]
  * Cambios en el paquete libai.ants.algorithms
  * Cambios en el paquete libai.ants Task #732 - Cambiar [TAB] por [SPACE]
  * Permitir extraer las matrices de las NN Task #731 - Permitir extraer las matrices de todos las ANN
  * Cambiar gitignore

1.0 / 2017-05-28
================

  * Merge pull request #68 from kronenthaler/fuzzy-improvements
  * wip: add IO operations to the FuzzyController
  * wip: remove old fuzzy package
  * wip: update fuzzy example
  * docs: add documentation about the fuzzy sets and defuzzifiers
  * wip: add Z,S,PI,Gaussians shapes
  * wip: add right/left linear shape
  * wip: fix codacy reports
  * wip: add delta step for the defuzzification process
  * wip: add fuzzy set shapes
  * wip: connected rulebase reasoning
  * wip: update travis to send coverage data to codacy
  * wip: add defuzzifier algorithms COA and COG
  * wip: add defuzzifier methods CoG and MoM
  * wip: improve minor issues from codacy
  * refactor: converted placeholder enums into class hierarchies
  * wip: refactor enums into interfaces and implementations
  * perf: remove unused statments
  * chore: fix test errors
  * chore: update travis to newer version
  * wip: add fuzzy controller xml parsing
  * wip: add rulebase xml parsing
  * wip: add antecedent, consequent, and rule XML parsing
  * wip: add clause xml type and improvements on parsing
  * wip: add fuzzy varaible and knowledge base xml serialization
  * wip: add fuzzy term and xml serialization
  * wip: new implementation of fuzzy engine
  * Merge pull request #67 from kronenthaler/svm-improvements
  * refactor: improve kernels coverage
  * feat: improvements on the SVM algorithm: generic kernel support, code quality improved
  * Merge pull request #66 from kronenthaler/preconditions
  * refactor: fix codacy issues
  * test: add test for new Shuffler class
  * test: remove warning regarding mysql driver from tests
  * test: add preconditions to the error functions
  * test: add preconditions on all train methods and refactor for clear types of learning
  * wip: reduce precondition checks by making types more strict
  * docs: reformat for standard code
  * test: improve coverage and formatting
  * wip: enforce matrix types on the nn training algorithms
  * wip: update to use Row types
  * wip: refactor tests and demos to use columns instead of matrix
  * wip: update networks to use more proper types row/columns
  * wip: updated multilayer perceptron to use column/rows when needed
  * wip: move Matrix class to independent package
  * wip: add Preconditions to the NeuralNetwork class to throw exception rather than assert them
  * Merge pull request #65 from kronenthaler/hopfield
  * test: removed unused import
  * feat: revised Hopfield network implementation
  * Merge pull request #64 from kronenthaler/kohonen-improvements
  * feat: improvements on the Kohonen network, prototype patterns are retrieved when simulate function is called instead of winner cell position
  * docs: update readme file
  * Merge pull request #63 from kronenthaler/hebb
  * test: improve codacy issues
  * refactor: Hebb network as supervised network.
  * Merge pull request #62 from kronenthaler/kohonen
  * test: remove unused imports
  * test: increase coverage of the Kohonen class
  * test: add kohnen tests
  * Merge pull request #61 from kronenthaler/codacy-issues
  * refactor: add Hebb error calculation to the training algorithm
  * refactor: rename some parameters to unify the conventions
  * refactor: cleanup some code and tests
  * refactor: remove inverted logic for the progress bars
  * Update README.md
  * Merge pull request #60 from kronenthaler/codacy-issues
  * refactor: move variables to the top
  * refactor: remove unused imports
  * refactor: random generator dependency injected in most places
  * test: fix some tests after refactor
  * perf: small improvements on the codebase based on codacy report
  * Merge pull request #59 from codacy-badger/codacy-badge
  * Add Codacy badge
  * Merge pull request #58 from kronenthaler/backpropagation
  * test: improve coverage after refactor
  * feat: extracted backpropagation implementation from the MultilayerPerceptron to allow multiple implementations
  * Merge pull request #57 from dktcoding/tests-random-and-misc-unsupervised
  * Remove unused imports in MySQLDataSetTest
  * Add random generator to Kohonen
  * Remove trailing spaces
  * Add test for LVQ
  * Add error test in Competitive
  * Remove debugging call
  * Add test for Competitive
  * Remove unused java.io imports in nn package
  * Use NeuralNetwork#random in Hebb
  * Use NeurarlNetwork#random in LVQ
  * Use NeurarlNetwork#random in Competitive
  * Merge pull request #56 from kronenthaler/nn-open
  * Merge pull request #53 from dktcoding/add-tests-textfiledataset
  * Fix indentation in TextFileDataSetTest
  * test: improve the unit test of the MySQLDataSet to create the database when running and clean it afterwards
  * refactor: promote the open method to the abstract class and provide overload for different input streams
  * Merge pull request #54 from dktcoding/add-test-mysqldataset
  * Merge pull request #48 from dktcoding/add-more-test-pair-triplet
  * Add create database in tavis.yml
  * Add tests for MySQLDataSet
  * Add MySQL Connector/J test dependency
  * Add MySQL service to travis
  * Add tests for Attributes with names
  * Add tests for TextFileDataSet
  * Test hashcode equality in Pair
  * Test hashcode equality in Triplet
  * Use Objects#hash() in Pair and Triplet
  * Merge pull request #51 from dktcoding/neuralnetwork-random
  * Merge pull request #52 from dktcoding/add-tests-genetic
  * Add initial tests for genetics
  * Adapt tests to use a Fixed random generator
  * Remove trailing spaces
  * Add Random constructor to SVM
  * Add Random constructor to RBF
  * Remove trailing spaces
  * Add Random constructor to MLP
  * Actually use random to create the matrices in Perceptron
  * Add Random constructor to Adaline
  * Add Random constructor to Perceptron
  * Use random in NeuralNetwork#shuffle()
  * Add Random to NeuralNetwork
  * Merge pull request #47 from dktcoding/add-more-tests-mlp
  * Merge pull request #49 from dktcoding/add-more-tests-rbf
  * Merge pull request #46 from dktcoding/add-more-tests-matrix
  * Merge pull request #45 from dktcoding/add-more-tests-svm
  * Merge pull request #44 from dktcoding/add-more-tests-functions
  * Use fixed random in SVMTest
  * Test hashcode equality
  * Fix copy paste error
  * Fix indentation
  * Remove trailing spaces
  * Add RBF Test based on example
  * Add triplet test
  * Aditional == test for Pair
  * Fix generics in Pair
  * Add equals and hashcode to Pair
  * Add tests for Pair
  * Add missing toString() test (JaCoCo)
  * Add missing tests for hashCode() (JaCoCo)
  * Add missing test for equals (JaCoCo)
  * Missing tests for dotProduct() (JaCoCo)
  * Fix: Asigning a value greater than MAX when training is complete
  * Add initial SVM test based on the example
  * Add HyperbolicTangent test
  * Add identity test
  * Add symmetric sign test
  * Add sign test
  * Add sigmoid test
  * Remove printStackTrace() (see ext. desc)
  * Test momentum without beta (MLP)
  * Fix: Asigning a value greater than MAX when training is complete
  * Add tests for training with progressbar
  * Add beta and null path tests (MLP)
  * Merge pull request #42 from kronenthaler/search-tests
  * refactor: move printing matrix as openoffice format to the MatrixIO class
  * test: add unit test for the search package (DFS, BFS, AStar)
  * test: add impossible case for the BFS search
  * Merge pull request #41 from kronenthaler/jacoco
  * chore: switch to jacoco as offers integration with netbeans editor.
  * Merge pull request #40 from kronenthaler/maven
  * chore: ignore demos package from the coverage report
  * chore: improved gitignore and pom.xml. Cleanup iris.data file as it should be a test-only file
  * docs: minor visual enhancement on the readme file
  * chore: add coveralls integration
  * refactor: moved Examples class into the demos package and add target on the pom.xml
  * feat: make the project maven compatible. travis script updated for it. Dependencies (.jar) are no longer necessary on the project itself
  * Merge pull request #39 from dktcoding/input-validation-nn
  * Make train abstract again and change validation to it's own method
  * Add input validation to euclideanDistance2(Matrix...)
  * Fix typo
  * Validate inputs in NeuralNetwork#train()
  * Remove asserts in euclideanDistance2(Matrix Matrix) as it breaks Kohonen
  * Merge pull request #38 from dktcoding/fix-horrible-mistake-in-last-pr
  * Assertions were enabled in last PR
  * Fix: assertions...
  * Enable assertions on JUnit
  * Add some asserts to error and euclideandistance2
  * Enable assertions in ant test
  * Use euclideandistance2() in error()
  * Add tests for zero error
  * Add some tests for EuclideanDistance and Shuffle
  * Make MatrixIOTest#eval() public and static
  * Use ThreadLocalRandom in shuffle()
  * Some JavaDoc "fixes"
  * Merge pull request #36 from dktcoding/input-validation-matrix
  * Misc fixes
  * Revert: fix typo in Adaline
  * Fix typos and indentation
  * Fix typo in Adaline
  * Merge pull request #35 from dktcoding/fix-javadoc
  * Revove trailing spaces
  * Remove trailing spaces LVQ
  * Fix JavaDoc LVQ
  * Remove trailing spaces Adaline
  * Fix JavaDoc Adaline
  * Assrt & JavaDoc for swap
  * Typo in JavaDoc
  * Asserts & JavaDoc for position
  * Extract getCol from for
  * Assert & JavaDoc for get/set row col
  * Assert, exception & JavaDoc for dotproduct (see ext desc)
  * Asserts & JavaDoc for copy()
  * Asserts & JavaDoc for apply
  * Asserts & JavaDoc for multiply (mat mat)
  * Fix typo
  * Asserts & JavaDoc for multiply
  * Asserts and JavaDoc for add & subtract
  * Fix typo in comment
  * Add input validation to matrix constructor
  * Finalize matrix attributes
  * Merge pull request #31 from dktcoding/ant-target-misc
  * Add Main-Class to ant jar target
  * Fix typo in build.xml and .gitignore
  * Merge pull request #29 from dktcoding/add-some-tests
  * Merge pull request #28 from dktcoding/misc-failsafe-changes
  * Remove docs/
  * Fix: typo in JavaDoc
  * Add missing ant targets
  * Update Git Ignore
  * Modify tests to use 2 output neurons
  * Add tests for BSF demo
  * Add a test for the MLP demo
  * Add a test for the Adaline demo
  * Add initial MLP test
  * Fix: another copy paste typo
  * Add messages to the assumes
  * Fix: copy paste typo
  * Make the check methods of MatrixIO public
  * Add initial Adaline Test
  * Add initial Perceptron Test
  * Revert "Remove unnecessary castings"
  * Update README.md
  * Merge pull request #27 from dktcoding/add-travis-ci
  * Add a serialVersionUID to all serializable files
  * Remove some unused imports
  * Remove unnecessary castings
  * Replace StringBuffer with StringBuilder
  * Close most streams in case of failure (see ext. comment)
  * Use diamond operator
  * Add missing @Override annotations
  * Remove unsued variables
  * Don't rely on the default encoding (decimal, thousands separators)
  * Use try with resources in all nn clasess
  * Implement Matrix#hashCode()
  * Fix: Matrix#equals()
  * Add .travis.yml
  * Add ant script
  * Make demos compile without SwingExtensions (not necessary in Java 6)
  * Add jUnit dependencies
  * Update project properties (read ext. description)
  * Update Netbeans build scripts
  * Merge pull request #26 from dktcoding/unsorted-nn-changes
  * Merge pull request #24 from dktcoding/fix-matrix-fill
  * Improve a bit some comments
  * Close InputStreams in case of failure
  * Remove some unused variables
  * Remove default constructor and finalize matrices
  * Remove unused import
  * Close OutputStream in case of failure
  * Remove star imports
  * Fix Matrix#fill()
  * Remove unsed imports from last patch
  * Merge pull request #22 from dktcoding/octave-bin-format
  * Fix indentation in MatrixIO
  * Create MatrixIO and add other basic write options
  * Fix some typos
  * Add Matrix#saveAsOctaveBin() and tests
  * Change source to 1.7
  * Merge pull request #20 from dktcoding/add-more-functions
  * Change weird pattern for less weird one
  * Merge remote-tracking branch 'upstream/master' into add-more-functions
  * Merge pull request #19 from dktcoding/fix-javadoc
  * Rename Gauss to Gaussian
  * Add parentheses to Sinc
  * Merge pull request #17 from dktcoding/change-seed-for-random
  * Add Gauss function
  * Add ArcTangent function
  * Add Sinc function
  * Fix minor typos here and there
  * Silence all warnings (not actually fixing)
  * Make JavaDoc Compile
  * Add test test for default fill(boolean) behavior
  * Merge pull request #16 from dktcoding/change-identity-population
  * Use Math.min() instead of ? :
  * Revert change in libai.ants.Environment
  * Create overloaded function for Random argument
  * Initial commit for changing long seed for Random
  * Minor nitpick when populating the identity matrix
  * Merge pull request #14 from dktcoding/add-matrix-unittests
  * Fix comment and test in MatrixTest
  * Re added the MySQL Connector/J
  * Merge pull request #13 from dktcoding/patch-1
  * Merge pull request #12 from dktcoding/add-copyright-notice
  * Add controls to some tests
  * Fix Typo
  * Add final test for now...
  * Almost done with the tests...
  * Add dot product tests
  * Some more tests
  * Add test for Matrix#position()
  * Add initial unit tests for Matrix
  * Add jUnit 4 dependency
  * Change currentTimeMillis() for nanoTime()
  * Added proper credits for clasess in the ant package
  * Added the License header to all the source files.
  * FIX layout of the HopfieldPanel
  * REMOVE getAccumulatedFrequencies from the data set class and passed to the actual tree.
  * FIX splitKeepingRelation for the Mysql Data set.
  * -- clean comments and typos
  * Update README.md
  * -- clean sample files
  * Merge pull request #3 from kronenthaler/datasource_refactor
  * CHANGE merging refactor to master packages.
  * FIX TextFieldDataSet to behave the same as the MySQLDataSet and the previous implementation.
  * ADD accumulated cache frequencies
  * FIX DataSet sorting getAccumulatedFrequencies to match original implementation
  * CHANGE fixes for the calculation of the accumulated frequencies and the tree itself
  * ADD method to display/create attributes with name
  * ADD implementations of the getAccumulatedFrequencies function
  * ADD getAccumulatedFrequency function to the dataset interface.
  * ADD lazy frequency initialization/retrieval
  * OPT infoAvgDiscrete calculation
  * FIX the behavior of the functions to match the legacy ones.
  * ADD split function for the categorical attributes using the sorted array.
  * OPT optimizing the encapsulated field with final
  * ADD all the same function for the TextFileDataSet
  * CHANGE replaced the constructors for the factory methods
  * ADD extra factories, the constructors will be turned package protected in the future.
  * ADD Gain Information structure
  * Merge branch 'datasource_refactor' of github.com:kronenthaler/libai into datasource_refactor
  * CHANGE C4.5
  * ADD allTheSameOutput and allTheSame methods to the interface and implementations
  * ADD allTheSameOutput and allTheSame methods to the interface and implementations
  * ADD factory method
  * First refactor steps for the C4.5 algorithm
  * ADD Subset constructor OPT on the query generation
  * CHANCE Cleaning of the dataset implementations, improve the MySQL and TextFile DataSets.
  * First refactor to the DataSet interface into something that can be used from multiple kind of sources.
  * Merge pull request #2 from kronenthaler/bayes
  * added draft (still incmplete) of the bayes network procedure
  * Minor refactors typo
  * Merge remote-tracking branch 'refs/remotes/origin/master' into bayes
  * Added Naive bayes Refactor of package trees into classifiers.trees Moved functions and kernels to pagkages inside common
  * Added Naive bayes Refactor of package trees into classifiers.trees Moved functions and kernels to pagkages inside common
  * Upgraded netbeans project files
  * Corrected misspelled words
  * Refactors: - package rename from net.sf.libai to libai - Removed dependency to JProgressBar from NeuralNetwork class - Removed static fields on NeuralNetwork class
  * Update Condition.java
  * Readme file for GitHub
  * Merge remote-tracking branch 'refs/remotes/origin/master'
  * Readme file for GitHub
  * Readme file for GitHub
  * Merge branch 'master' of github.com:kronenthaler/libai
  * Initial commit
  * Trees: Pass from experimental to beta
  * Experimental: - Prune for C4.5: laplace and confidence mode.
  * Trees en modo experimental - Algoritmo C4.5 funciona bien, sin podas - Podas mecanismo base implementado pero esta fallando, no esta podando nada o las podas son muy radicales y producen mucho error a posteriori. - Faltan copiar los metodos de cargar/guardar que estaban implementados en la otra version del arbol - Falta modificar el data set para que genere subsets balanceados para la validacion cruzada.
  * NN: - SVM: trainning using the SMO algorithm implementation. Still optimization are pending. - Changed all the save/open to the java serialization system. Easier and safe.
  * SVM: - Simplified the training. (could be more inefficient, but is more generic) - Methods to save and open. - Changed some structures to be more efficient (arraylist converted to arrays when was possible) - Added some kernel implementations, linear, polynomial, sigmoidal. The gaussian kernel remains out for issues related with the optimizations of the algorithm.
  * First version of the SVM algorithm, took from the implementation of svm-java from the site www.kernel-machines.org Some adaptations was maded, using Matrix class instead the bi-dimensional arrays for performance. Still miss the save/open functions. Also, still to be optimized whenever is possible.
  * First classes for the packages trees and search. In the package trees. review the performance of the data set In the package search just are implemented the BFS, DFS and A*. Examples just for BFS and A*. A* has efficiency problems for the priority queue and the remove steps of the algorithm.
  * -Added files for webstart support in the examples.
  * - Added Hopfield Networks. - Added example to the hopfield networks. - Bug fixed in the signum function and symmetric signum.
  * NN: - Implemented Rprop for the MLP network!! - RBF, modified to use an actual adaline network without overhead in the matrix creations.
  * ant-target to generate the releases
  * Documentation generated
  * - Documentation for: Hebb, Competitive and Kohonen. - NN: Incorporated the shuffle pattern mechanism for all the algorithms - Demos for the fuzzy and NN, still miss the genetics.
  * Documentation: left Hebb and Competitive TODO: demos
  * Initial import: - integrados los proyectos fuzzy, genetic, y nn en una sola libreria - NN: parcialmente portado de la libreria original en C++. Faltan los metodos de guardar y abrir de algunas redes. - Faltan los demos de los paquetes de geneticos y fuzzy. - Falta el demo de la clase Hebb en NN.
