----------
> # ⚠️ Disclaimer
> This is a **modified** version of [`libai`](https://github.com/kronenthaler/libai) which may
> contain changes that will most likely create problems for regular 
> [`libai`](https://github.com/kronenthaler/libai) users.
> 
> We will change this fork to fit our specific needs, consider using the original
> [`libai`](https://github.com/kronenthaler/libai) which will always have the latest features 
> and it will be more stable, updated and backwards compatible.

-----

LibAI 
=====

A library compiling Artificial Intelligence algorithms:

### Neural Networks
* Supervised: 
 - Perceptron (Single and Multilayer)
 - Adaline
 - Learning Vector Quantization
 - Radial Basis Function
 - Support Vector Machines.
 - Hebb

* Unsupervised: 
 - Competitive
 - Hopfield
 - Kohonen
	
### Fuzzy Logic
Inference engine, rules and set.
	
### Genetic Algorithm
Binary & permutation chromosomes, elitism algorithm.
	
### Decision Tree
* C4.5 algorithm with discrete and continuous attributes.
* Naive Bayes inference.
	
### Search Algorithms
* Depth First Search
* Breeadth First Search
* A*
	
### Ant Colony Optimization
* Metaheuristic framework
* MMAS
* AntQ
* Ant System
* Ant System Rank
* Elistist Ant System.

More things to come!
	
