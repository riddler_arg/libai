/*
 * MIT License
 *
 * Copyright (c) 2009-2016 Enrique Areyan <enrique3 at gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package libai.ants.algorithms;

import java.util.List;
import libai.ants.Ant;
import libai.ants.Enviroment;

/**
 * This class belong to the core classes of the Ant Framework.
 * <p>
 * Implements the Rank Ant System algorithm. It is essentially the same as
 * Elitist Ant System, but the arcs of the best-so-far tour of the best
 * {@code r} ants are reinforced proportionally to the ant's rank Empirical
 * results shows that this behaves slightly better that Elitist Ant System.
 *
 * @author Enrique Areyan, enrique3 at gmail.com
 * @version 1
 */
public abstract class AntSystemRank extends ElitistAntSystem {

    /**
     * Constructor. Allocates the enviroment.
     *
     * @param E enviroment
     */
    protected AntSystemRank(Enviroment E) {
        super(E);
    }

    /**
     * Constructor. Empty constructor.
     */
    protected AntSystemRank() {
    }

    @Override
    public void pheromonesUpdate() {
        /* order ants*/
        E.sortAnts(this);
        /* calculated amount of elitist ants */
        final int numberOfElitistAnts = Math.min(this.numberOfAnts, this.Parameters.get(AntSystemRank.EPSILON).intValue() - 1);
        /* forach elitist ant */
        for (int k = 0; k < numberOfElitistAnts; k++) {
            final Ant a = this.Ants[k];
            final List<Integer> solution = a.getSolution();
            final double contribution = Math.max(0, this.Parameters.get(AntSystemRank.EPSILON) - 1 - k) * (1 / f(solution));
            //System.out.println("Contribution of Ant["+k+"] = "+contribution);
            /* Add contribution to each arc of this ant's solution */
            for (int i = 0; i < solution.size() - 1; i++) {
                final int node_i = solution.get(i);
                final int node_j = solution.get(i + 1);
                this.Pheromones.increment(node_i, node_j, contribution);
            }
        }
    }
}
