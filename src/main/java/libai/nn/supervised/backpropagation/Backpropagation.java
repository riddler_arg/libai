package libai.nn.supervised.backpropagation;

import libai.common.matrix.Column;
import libai.common.matrix.Matrix;
import libai.common.functions.Function;
import libai.nn.NeuralNetwork;

/**
 * Created by kronenthaler on 08/01/2017.
 */
public interface Backpropagation {

    /**
     * Initializes the Backpropagation algorithm with the basic structures
     * needed. This method usually should be called in the NeuralNetwork's train
     * method right before calling the Backpropagation's train method.
     * @param nn Neural Network
     * @param nperlayer Array containing the number of neurons per layer
     * @param functions Activations functions
     * @param W Weight matrix
     * @param Y ..
     * @param b ..
     * @param u ..
     * @see libai.nn.supervised.Adaline
     * @see libai.nn.supervised.Hebb
     * @see libai.nn.supervised.LVQ
     * @see libai.nn.supervised.MultiLayerPerceptron
     * @see libai.nn.supervised.Perceptron
     * @see libai.nn.supervised.RBF
     * @see libai.nn.supervised.SVM
     */
    void initialize(NeuralNetwork nn, int[] nperlayer, Function[] functions, Matrix[] W, Column[] Y, Column[] b, Column[] u);

    /**
     * Trains a neural network using the backpropagation implementaion.
     * @param patterns Patterns to train (input data)
     * @param answers Pattern solutions (output data)
     * @param alpha Training coefficient
     * @param epochs number of epochs to train
     * @param offset Pattern offset
     * @param length Number of patterns
     * @param minerror Minimization error
     */
    void train(Column[] patterns, Column[] answers, double alpha, int epochs, int offset, int length, double minerror);
}
