package libai.common.matrix;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Row class
 *
 * @author kronenthaler on 12/03/2017.
 * @see Column
 * @see Matrix
 */
public class Row extends Matrix {
    private static final long serialVersionUID = 471750854178537656L;

    /**
     * Creates a new row
     *
     * @param c number of columns
     */
    public Row(int c) {
        super(1, c);
    }

    /**
     * Creates a new row
     *
     * @param data row array
     */
    public Row(double[] data) {
        super(1, data.length, data);
    }

    /**
     * Copy constructor.
     *
     * @param copy Row to copy
     */
    public Row(Row copy) {
        super(copy);
    }

    /**
     * Retrieves the value of a given column.
     *
     * @param i column number
     * @return value
     * @since 1.2.0-RIDDLER
     */
    public double get(int i) {
        return position(0, i);
    }

    /**
     * Sets the value for a given column.
     *
     * @param i column number
     * @param val value
     * @since 1.2.0-RIDDLER
     */
    public void set(int i, double val) {
        position(0, i, val);
    }

    /**
     * New random row.
     *
     * @param c number of columns
     * @return new Random {@link Column}
     */
    public static Row random(int c) {
        return random(c, true);
    }

    /**
     * New random row.
     *
     * @param c number of columns
     * @param signed {@code false} if all the numbers should be positive,
     * {@code false} otherwise
     * @return new Random {@link Column}
     */
    public static Row random(int c, boolean signed) {
        return random(c, signed, getDefaultRandom());
    }

    /**
     * New random row.
     *
     * @param c number of columns
     * @param signed {@code false} if all the numbers should be positive,
     * {@code false} otherwise
     * @param rand The {@link Random} object used to fill the matrix, if
     * {@code null} it will fallback to {@link ThreadLocalRandom#current()}
     * @return new Random {@link Column}
     */
    public static Row random(int c, boolean signed, Random rand) {
        Row ret = new Row(c);
        ret.fill(signed, rand);
        return ret;
    }

    @Override
    public Column transpose() {
        Column result = new Column(columns);
        transpose(result);
        return result;
    }

    public void transpose(Column result) {
        System.arraycopy(matrix, 0, result.matrix, 0, matrix.length);
    }
}
