package libai.common.matrix;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Column class
 *
 * @author kronenthaler on 12/03/2017.
 * @see Row
 * @see Matrix
 */
public class Column extends Matrix {
    private static final long serialVersionUID = 836410303513443555L;

    /**
     * Creates a new column
     *
     * @param r number of rows
     */
    public Column(int r) {
        super(r, 1);
    }

    /**
     * Creates a new column
     *
     * @param data column array
     */
    public Column(double[] data) {
        super(data.length, 1, data);
    }

    /**
     * Copy constructor.
     *
     * @param copy Column to copy
     */
    public Column(Column copy) {
        super(copy);
    }

    /**
     * Retrieves the value of a given row.
     *
     * @param i row number
     * @return value
     * @since 1.2.0-RIDDLER
     */
    public double get(int i) {
        return position(i, 0);
    }

    /**
     * Sets the value for a given row.
     *
     * @param i row number
     * @param val value
     * @since 1.2.0-RIDDLER
     */
    public void set(int i, double val) {
        position(i, 0, val);
    }

    /**
     * New random column.
     *
     * @param r number of rows
     * @return new Random {@link Column}
     */
    public static Column random(int r) {
        return random(r, true);
    }

    /**
     * New random column.
     *
     * @param r number of rows
     * @param signed {@code false} if all the numbers should be positive,
     * {@code false} otherwise
     * @return new Random {@link Column}
     */
    public static Column random(int r, boolean signed) {
        return random(r, signed, getDefaultRandom());
    }

    /**
     * New random column.
     *
     * @param r number of rows
     * @param signed {@code false} if all the numbers should be positive,
     * {@code false} otherwise
     * @param rand The {@link Random} object used to fill the matrix, if
     * {@code null} it will fallback to {@link ThreadLocalRandom#current()}
     * @return new Random {@link Column}
     */
    public static Column random(int r, boolean signed, Random rand) {
        Column ret = new Column(r);
        ret.fill(signed, rand);
        return ret;
    }

    @Override
    public Row transpose() {
        Row result = new Row(rows);
        transpose(result);
        return result;
    }

    public void transpose(Row result) {
        System.arraycopy(matrix, 0, result.matrix, 0, matrix.length);
    }

}
